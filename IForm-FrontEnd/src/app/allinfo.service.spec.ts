import { TestBed } from '@angular/core/testing';

import { AllinfoService } from './allinfo.service';

describe('AllinfoService', () => {
  let service: AllinfoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AllinfoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
