import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { RegistrationService } from '../registration.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})
export class RegisterComponent implements OnInit {
  public successMessage: any;
  public shimali: any;
  public loaderStatus: any = false;
  public dataToBeSent: any;
  public data: any;
  public val: any;
  constructor(
    private fb: FormBuilder,
    private _registrationService: RegistrationService,
    private router: Router
  ) {}
  registrationForm = this.fb.group({
    first_name: ['', Validators.required],
    last_name: ['', [Validators.required]],
  });
  onSubmit() {
    let token = localStorage.getItem('token');
    this.dataToBeSent = {
      first_name: this.registrationForm.value.first_name,
      last_name: this.registrationForm.value.last_name,
      token: token,
    };
    this.loaderStatus = true;

    this._registrationService.register(this.dataToBeSent).subscribe(
      (response) => {
        this.loaderStatus = false;
        alert('successfully created');
        this.router.navigate(['/allinfo']);
      },
      (error) => console.log(error)
    );
  }

  ngOnInit(): void {}
}
