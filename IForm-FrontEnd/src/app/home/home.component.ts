import { Component, OnInit } from '@angular/core';
import { GeneratetokenService } from '../generatetoken.service';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
})
export class HomeComponent implements OnInit {
  public alertStatus = false;
  public loader = false;
  onGenerate() {
    this.loader = true;
    this._generateTokenService.generateToken().subscribe(
      (response) => {
        this.loader = false;
        this.alertStatus = true;
        localStorage.setItem('token', response.access_token);
      },
      (error) => console.log(error)
    );
  }
  constructor(private _generateTokenService: GeneratetokenService) {}

  ngOnInit(): void {}
}
