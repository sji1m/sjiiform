import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class RegistrationService {
  constructor(private _http: HttpClient) {}
  register(data: any) {
    return this._http.post('http://localhost/iform/createrecord.php', data);
  }
}
