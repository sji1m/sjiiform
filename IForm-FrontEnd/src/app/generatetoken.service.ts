import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class GeneratetokenService {
  _url = 'http://localhost/iform/index.php';
  constructor(private _http: HttpClient) {}
  generateToken() {
    return this._http.post<any>(this._url, null);
  }
}
