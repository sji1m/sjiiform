import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class DetailsService {
  getDetails(token: any, id: any) {
    return this._http.get<any>('http://localhost/iform/extrainfo.php', {
      headers: new HttpHeaders({
        Token: token,
        Id: id,
      }),
    });
  }
  constructor(private _http: HttpClient) {}
}
