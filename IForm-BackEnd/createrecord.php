<?php 
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: *");
header('Content-Type: application/json; charset=utf-8');
 $data2 = json_decode(file_get_contents('php://input'), true);
 

$url = 'https://loadapp.iformbuilder.com/exzact/api/v60/profiles/372748/pages/290840779/records';
$data = array(
    "fields" => array (
        array(
		  "element_name" => "first_name",
		  "value" => $data2['first_name'],
		),
		array(
		  "element_name" => "last_name",
		  "value" => $data2['last_name'],
		)
        
    )
);

$postdata = json_encode($data);

$ch = curl_init($url); 
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
'Content-Type: application/json',
'Authorization: Bearer '.$data2["token"]
)
);
$result = curl_exec($ch);
curl_close($ch);
print_r ($result);