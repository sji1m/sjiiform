import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DetailsService } from '../details.service';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css'],
})
export class DetailsComponent implements OnInit {
  public record_id: any;
  public first_name: any;
  public last_name: any;
  public id: any;
  public loaderStatus: any = true;
  public cardStatus: any = false;
  constructor(
    private route: ActivatedRoute,
    private _detailsService: DetailsService
  ) {
    let id = this.route.snapshot.paramMap.get('id');
    let token = localStorage.getItem('token');
    this._detailsService.getDetails(token, id).subscribe(
      (response) => {
        this.loaderStatus = false;
        this.cardStatus = true;
        this.record_id = response.id;
        this.first_name = response.first_name;
        this.last_name = response.last_name;
      },
      (error) => console.log(error)
    );
  }

  ngOnInit(): void {}
}
