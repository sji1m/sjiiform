import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
@Injectable({
  providedIn: 'root',
})
export class AllinfoService {
  getAllUsers(token: any) {
    return this._http.get<any>('http://localhost/iform/allrecord.php', {
      headers: new HttpHeaders({
        Token: token,
      }),
    });
  }
  constructor(private _http: HttpClient) {}
}
