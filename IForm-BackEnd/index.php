<?php 
header("Access-Control-Allow-Origin: *");
header('Content-Type: application/json; charset=utf-8');

date_default_timezone_set('Asia/Dhaka');
function base64url_encode($text)
{
    return str_replace(
        ['+', '/', '='],
        ['-', '_', ''],
        base64_encode($text)
    );
}

$headers = [ "alg" => "HS512", "typ" => "JWT"];

$headers_encoded = base64url_encode(json_encode($headers));
$issuedAt = time();
$expires_at = time()+300;

    $payload =  [
        "iss" =>"3e889bc8316e66e9aaca86d0db44ada89c6a18af",
        "aud"=> "https://loadapp.iformbuilder.com/exzact/api/oauth/token", //Subject
        "exp"=> $expires_at,
        "iat"=> $issuedAt,  
 ];

      $payload_encoded = base64url_encode(json_encode($payload));
	  
	  $key = "534000864037a89999893e1bd3e93cdf3977c60b"; 
$signature = hash_hmac('SHA512',"$headers_encoded.$payload_encoded",$key,true);
$signature_encoded = base64url_encode($signature);

$token = "$headers_encoded.$payload_encoded.$signature_encoded";
$array = [

];
function httpPostXform($url, $data) {                                                           
    $curl = curl_init($url);                                                                            
    curl_setopt($curl, CURLOPT_POST, true);                                                             
    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));                                    
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $headers = array(
    "Access-Control-Allow-Origin: *",
	"Content-Type: application/x-www-form-urlencoded",
    );	
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);   
    $response = curl_exec($curl);
	
    curl_close($curl);                                                                                  
    return $response;                                                                      
}                                                                                                       
$r = httpPostXform("https://loadapp.iformbuilder.com/exzact/api/oauth/token",array("grant_type"=>"urn:ietf:params:oauth:grant-type:jwt-bearer","assertion"=>$token));
$exploded_value= explode(",",$r);
$access_token = substr($exploded_value[0],17,40);
$json = [
  "access_token" => $access_token
];
echo json_encode($json);
