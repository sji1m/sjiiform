import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AllinfoService } from '../allinfo.service';
@Component({
  selector: 'app-allinfo',
  templateUrl: './allinfo.component.html',
  styleUrls: ['./allinfo.component.css'],
})
export class AllinfoComponent implements OnInit {
  public data: any;
  public loaderStatus: any = true;
  constructor(private _allinfoService: AllinfoService, private router: Router) {
    let token = localStorage.getItem('token');
    this._allinfoService.getAllUsers(token).subscribe(
      (response) => {
        this.loaderStatus = false;
        this.data = response;
      },
      (error) => console.log(error)
    );
  }
  onClick(individual_data: any) {
    this.router.navigate(['/details', individual_data.id]);
  }

  ngOnInit(): void {}
}
